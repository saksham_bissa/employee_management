<?php
$name=$email=$gender=$password=$designation=$hobbies=$address=$image="";  //initialization of php variables
$nameErr=$emailErr=$genderErr=$passwordErr=$designationErr=$hobbiesErr=$addressErr=$imageErr="";   //Initialization of error variables;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["name"])) {       //condition if the name feild is empty
    $nameErr = "Name is required";
  } else {
          $name = test_input($_POST["name"]);
   
   
  }
    if (empty($_POST["email"])) {           //check if the email feild is empty
    $emailErr = "Email is required";
  } 
       else {
         $email=$_POST["email"];
         $count1=substr_count("$email","@");  //count no. of occurance of @
         $count2=substr_count("$email",".");  //count no. of occurance of .
         if($count2 >=3) {
         $emailErr = "Invalid Email Format..";
  }
             else {
           $email=test_input($_POST["email"]);
  }
  }

 /*
 if (empty($_POST["gender"])) {                //validation of gender feild
    $genderErr = "Gender is required";
    
  } else {
    $gender= test_input($_POST["gender"]);
  }
*/

    if (empty($_POST["password"])) {              //validation of password feild
    $passwordErr = "Password is required";
  } else {
        $pswd_len=strlen($_POST["password"]);     //checking the length of password
         if($pswd_len < 6 || $pswd_len >10) {
        $passwordErr = "Password be alpha numeric of Length between(6-10)";
   
}
         else {
         $temp_pass=$_POST['password'];
         $index=substr("$temp_pass",0);       //fetching first character of the pasword
         $cap=ucwords($index);
         if($index >='A' && $index<='Z')        //checking first character of password is a Upper case alphabet.
         $password= test_input($_POST["password"]);
         else
         $passwordErr = "Password should be in alpha numeric style with Capital letter at the begining";
          
}      
  }

     if ($_POST["designation"]=="NA") {          //designation feild validation
    $designationErr = "Designation is required";
  } else {
    $designation= test_input($_POST["designation"]);
  }

    if (empty($_POST["address"])) {                 //address feild validation
    $addressErr = "Address is required";
  } else {
    $address= test_input($_POST["address"]);
  }
   if (empty($_FILES["image"]["name"])) {                  //image feild validation
    $imageErr = "Profile pic required ";
  } else {
    
    $path = $_FILES['image']['name'];
    $ext = pathinfo($path, PATHINFO_EXTENSION);
    if($ext=="jpg" || $ext=="png" ||$ext=="jpeg")       //image extention validation
    $image= test_input($_FILES["image"]["name"]);
    else
    $imageErr = "Please Upload jpg/png format image only ";
  }

if (empty($_POST["hobbies"])) {                        //hobbies feild validation
    $hobbiesErr = "Hobbies are required ";
  } 
//$image=test_input($_POST["image"]);
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>

<?php //Database insertion block
if(isset($_POST["submit"]) && (!empty($_POST["name"]))&& (!empty($_POST["email"])) && (!empty($_POST["password"])) && (!empty($_POST["hobbies"]))  && (!empty($_POST["address"])) && (!empty($_FILES["image"]["name"])) )
{
include 'config.php';
extract($_POST);
$hobbies=implode(',', $_POST['hobbies']);
$path="images/".$_FILES["image"]["name"];
$password=md5($password);
//$date=date('Y-m-d H:i:s');
$date = date('Y-m-d', time());
move_uploaded_file($_FILES["image"]["tmp_name"],$path);

if ($_FILES['image']['tmp_name'] != '' )
{
$query="INSERT INTO `registration` (`name`,`image`,`email`,`gender`,`password`,`designation`,`hobbies`,`address`,`created_at`) VALUES ('".$name."','".$path."','".$email."','".$gender."','".$password."','".$designation."','".$hobbies."','".$address."','".$date."')";
// echo $query; exit;
mysqli_query($conn,$query) or die(mysqli_error($conn));
/*echo'<script>alert("Employee Registration Successfully !!");
window.location.href="form.php";
</script>';*/
$query1=mysqli_query($conn,"SELECT * FROM `registration` ");
$page=mysqli_num_rows($query1);
$page=$page/8;
$page=ceil($page);
header("location:employee_listing.php?page=$page&message=Congrats! Employee Registration Successful");

}


}




?>
<!DOCTYPE html>
<html>
<head>
<title>Employee Form</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script src="javascript/registration-form-validation.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
.form {

}
.error
{
color:red;
}
</style>
<script>
function fun_hobbies()
{
    if(document.getElementById("all_hobbies").checked = true)
    {
    for(i=1;i<=11;i++)
    {
    document.getElementById(i).checked = true;
    }
  
    }
    

}
</script>



</head>
<body>

<?php include 'header2.php'?>
    <header>
        <div class="container-fluid">
             <div class="row ">
                 <div class="col-md-12 col-sm-12">
                  <center><h3><u>Employee Registration Form</u></h3></center>
                  <br>
                  <br>
                 </div>

                  <!--<div class="col-md-3 col-sm-12">
                  <div class="dropdown">
    <span class=" dropdown-toggle" type="" data-toggle="dropdown"><b>Edit Preferences</b>
    <span class="caret"></span></span>
    <ul class="dropdown-menu">
      <li><a href="#">Personal details</a></li>
      <li><a href="#">Professional details</a></li>
      <li><a href="#">Communication details</a></li>
    </ul>
  </div>
  
                  </div>-->
                  <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post" enctype="multipart/form-data"  >
                  <div class="col-md-3 col-sm-12  " >
                  <fieldset>
                      <legend>Personal Details</legend>
                   
                         <div class="form-group">
                             <label for="name">
                              Enter Name <span class="error">* </span> </span>  <span class="error" id="nameError"> </span>
                             </label>
                             <input type="name" id="name" name="name" placeholder="Your Name" oninput="return nameValid()" class="form-control"/>
                             <span class="error"> <?php echo $nameErr;?></span>
                         </div>

                         <div class="form-group">
                             <label for="gender">
                              Select Gender <span class="error"> </span>
                             </label>
                              <br>
                             <input type="radio" id="gender" checked="checked" name="gender" value="Male"/>Male
                             <br>
                             <input type="radio" id="gender" name="gender" value="Female"/>Female
                         </div>
                         <div class="form-group">
                             <label for="password">
                              Choose Your Password<span class="error">* </span> <span class="error" id="passwordError"> </span>
                             </label>
                             <input type="password" id="password" name="password" placeholder="Your Password" class="form-control" oninput="return passwordValid()"/>
                             <span class="error"><?php echo $passwordErr;?>
                         </div>



                     
                  </fieldset>
                 </div>
             <div class="col-md-3">
<fieldset>
<legend>
Professional Details
</legend>
<div class="form-group">
                             <label for="designation">
                              Select Designation<span class="error">* </span> <span class="error" id="designationError"> </span>
                             </label>
                             <select class="form-control" name="designation" id="designation">
                                 <option  value="NA" >Select</option>
                                 <option value="Associate Software Engineer">Associate Software Engineer</option>
                                 <option value="Software Engineer">Software Engineer</option>
                                 <option value="Sr. Software Engineer">Sr. Software Engineer</option>
                                 <option value="Project Manager">Project Manager</option>
                                 <option value="Technology Lead">Tech.noloy Lead</option>
                                 <option value="System Analyst">System Analyst</option>
                                 <option value="Bussiness Analyst">Bussiness Analyst</option>
                                 <option value="HR Executive">HR Executive</option>
                                 <option value="SR. HR Executive">Sr. HR Executive</option>
                             </select>
                             <span class="error"><?php echo $designationErr;?></span>
                         </div>
</fieldset>
             </div>
             <div class="col-md-3">
<fieldset>
<legend>
Communication Details
</legend>
<div class="form-group">
                             <label for="Email">
                              Enter Email Id <span class="error">* </span> <span class="error" id="emailError"> </span>
                             </label>
                             <input type="email" id="email" name="email" placeholder="Your Email" oninput="return emailValid()" class="form-control"/>
                             <span class="error"><?php echo $emailErr;?></span>
                         </div>
                         <div class="form-group">
                             <label for="address">
                              Communication Address<span class="error">* </span> <span class="error" id="addressError"> </span>
                             </label>
                             <textarea name="address" id="address" class="form-control"></textarea>
                             <span class="error"><?php echo $addressErr;?></span>
                         </div>
</fieldset>
             </div>
             <div class="col-md-3">
<fieldset>
<legend>
Miscleneous
</legend>

<div class="col-md-12">
                        <div>
                            <img id="user_img"
                                 height="130"
                                 width="130"
                                 style="border:solid" />
                        </div>
                       <br>
                       <br>
                    </div>
                         <div class="form-group col-md-12">
                             <label for="image">
                              Choose Profile Picture <span class="error">* </span><span class="error" id="imageError"> </span>
                             </label>
                             <input type="file" name="image" id="file" class="form-control" onchange="return show(this)"/>
                            <span class="error"> <?php echo $imageErr;?></span>
                         </div>
                     
 
                    <div class="form-group">
                             <label for="hobbies">
                              Choose Your Hobbies <span class="error">*</span><span class="error"><?php echo $hobbiesErr;?></span>
                             </label>
                             <br>
                             <input type="checkbox" id="1" name="hobbies[]" value="Playing"/>Playing
                             
                             <input type="checkbox" id="2" name="hobbies[]" value="Singing"/>Singing
                             
                             <input type="checkbox" id="3" name="hobbies[]" value="Dancing"/>Dancing
                             
                             <input type="checkbox" id="4" name="hobbies[]" value="Watching Movies"/>Watching Movies
                             
                             <input type="checkbox" id="5" name="hobbies[]" value="Reading Novels"/>Novels
                             
                             <input type="checkbox" id="6" name="hobbies[]" value="Cooking"/>Cooking
                             
                             <input type="checkbox" id="7" name="hobbies[]" value="Travelling"/>Travelling
                             
                             <input type="checkbox" id="8" name="hobbies[]" value="Eating"/>Eating
                             
                             <input type="checkbox" id="9" name="hobbies[]" value="Internet Surfing"/>Internet Surfing
                             
                             <input type="checkbox" id="10" name="hobbies[]" value="Social Media"/>Social Media
                             
                             <input type="checkbox" id="11" name="hobbies[]" value=" and many more."/>Many more..
                             <br><br> 
                             <input type="checkbox" id="all_hobbies" onclick="return fun_hobbies()" /><b id="b1"> Select All</b>
                             <span class="error" id="hobbiesError"> </span>
                         </div>
</fieldset>
             </div>
             
             <div class="col-md-4 " ></div>
             <div class="col-md-4 " >
             <div class="form-group" >
             <br>
             <br>
             <br>
<input type="submit" name="submit" id="submit" onclick="return funOnClick()" class="form-control btn-success"/>
</div>
<center>
<br>

<h5><strong><a href="index.php">Already Registered! Click here to login. </a></strong></h5>




</center>
             </div>
                 </form>
             </div>
        </div>
      
    </header>
<?php include 'footer2.php'?>
<body>


</html>
