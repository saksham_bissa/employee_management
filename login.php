<?php session_start(); ?>
<?php
$name=$email=$gender=$password=$designation=$hobbies=$address=$image="";  //initialization of php variables
$emailErr2=$emailErr=$genderErr=$passwordErr=$designationErr=$hobbiesErr=$addressErr=$imageErr=$passwordErr2="";   //Initialization of error variables;
if ($_SERVER["REQUEST_METHOD"] == "POST") {
 
    if (empty($_POST["email"])) {           //check if the email feild is empty
    $emailErr = "Email is required";
  } 
       else {
         $email=$_POST["email"];
         $count1=substr_count("$email","@");  //count no. of occurance of @
         $count2=substr_count("$email",".");  //count no. of occurance of .
         if($count2 >=3) {
         $emailErr = "Invalid Email Format..";
         $emailErr2 = "";
  }
             else {
           $email=test_input($_POST["email"]);
  }
  }

    if (empty($_POST["password"])) {              //validation of password feild
    //$passwordErr = "Password is required";
  } else {
        $pswd_len=strlen($_POST["password"]);     //checking the length of password
         if($pswd_len < 6 || $pswd_len >10) {
        $passwordErr = "Password be alpha numeric of Length between(6-10)";
   
}
         else {
         $temp_pass=$_POST['password'];
         $index=substr("$temp_pass",0);       //fetching first character of the pasword
         $cap=ucwords($index);
         if($index >='A' && $index<='Z')        //checking first character of password is a Upper case alphabet.
         $password= test_input($_POST["password"]);
         else
         $passwordErr = "Password should be in alpha numeric style with Capital letter at the begining";
          
}      
  }

}
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}//feild validation ends
?>
<?php //Login Query Block starts
include 'config.php';
extract($_POST);
if(isset($_POST['login_submit']) && (!empty($_POST['email']))  )
{
    $email_query="SELECT `email` FROM `registration` WHERE `email` = '".$email."'";
    $ed=mysqli_query($conn,$email_query);
    @$num_rows=mysqli_num_rows($ed);
    if(@$num_rows <= 0 )
    {
        $emailErr2="Email is not registered with us.";

    }

  if(@$num_rows >0  && (!empty($_POST['password'])) )
    {
    $password=md5($password);
    $log_query="SELECT `email`,`password` FROM `registration` WHERE `email` = '".$email."' and `password`= '".$password."'  ";
    $data=mysqli_query($conn,$log_query);
    $result=mysqli_fetch_array($data);
    if($email== $result["email"] and $password ==$result["password"] )
    {
        $_SESSION["email"]=$email;
        $_SESSION["password"]=$password;
        header("location:dashboard.php");
    }
    else
    {
    
     $passwordErr2="Incorrect password..";
    }  
} 
}
//login Query Block ends here.
?> 
<!DOCTYPE html>
<html>
<head>
<title>Login ! User</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<style>
.form {
float:center;
}
.error
{
color:red;
}
</style>
</head>
<body>
  <?php include 'header2.php'?>
<header>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
               <center><h4>Employee Login Panel</h4></center>
               <br>
             
               
            </div>
        </div>
    </div>
</header>
<section class="content_wrapper">
    <div class="container">
        <div class="row">
          <div class="col-md-3 col-sm-3"></div>
            <div class="col-sm-6 col-md-6 bg-success" style=";border:.5px solid silver;">
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post"style="padding:20px">
                <?php
                 if(@$num_rows>0)
                  {
                  ?> 
                     <center><b>Welcome Back.</b>
                     <br>
                     <br>
                     <strong><input type="text" name="email" value="<?php echo $email; ?>" style="border:.8px solid black;font-size:2em;border-radius:50px;color:gray;padding:10px;text-align:center;" readonly="readonly"/></strong>
                     </center> 
                     <br>
                     <br>
                     <div class="form-group">
                        <label>
                        Enter Password <span class="error">* </span>
                        </label>
                        <input type="password" class="form-control" placeholder="Your password" name="password" style="border-radius:50px"/>
                        <span class="error"><?php echo $passwordErr;?></span><br>
                        <span class="error"><?php echo $passwordErr2;?></span>
                         </div>
                            
                  <?php 
                  }
                  else
                  {
                  ?>
                    <div class="form-group">
                        <label>
                         Enter registered email Id <span class="error">* </span>
                        </label>
                        <input type="email" class="form-control" placeholder="Your Email id" name="email" />
                        <span class="error"><?php echo $emailErr;?></span>
                        <br>
                        <span class="error"><?php echo $emailErr2;?></span>
                    </div>
                  <?php }?>

                  <div class="form-group">

                        <input type="submit" class="form-control btn btn-success" value="Secure Login !"  name="login_submit"/>
                  </div> 
                </form>

            </div>
 
        </div>
        
                  <center>

                  <strong > <a href="forget-password.php" >Forgot Password ! </a></strong>
                    <h5><strong><a href="form.php">New Employee ! Register Here. </a></strong></h5>
                    <h4>

                
                       
                   </center>
        
    </div> 


</section>
<?php include 'footer2.php' ?>
</body>
</html>