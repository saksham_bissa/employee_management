/////////////////////////////Client Side Validation for Name Feild starts here
function nameValid() {
    var name = document.getElementById("name").value;
    document.getElementById("nameError").innerHTML = "";
    var pos = name.charAt(0);
    if ((name >= 'a' && name <= 'z') || (name >= 'A' && name <= 'Z')) {
        if (!(pos >= 'A') || !(pos <= 'Z')) {
            document.getElementById("nameError").innerHTML = "First letter should be capital";
            document.getElementById("name").value = "";
            return false;
        }


    } else {
        document.getElementById("nameError").innerHTML = "Only characters Allowed";
        document.getElementById("name").value = "";
        return false;
    }
}

/////////////////////////////Client Side Validation for Name Feild ends here



/////////////////////////////Client Side Validation for Email  Feild starts here
function emailValid() {
    var email = document.getElementById("email").value;
    var pos = email.charAt(0);
    var count = (email.match(/@/g) || []).length;
    // var count2 = (email.match(/./g) || []).length;
    var index_of_at = email.indexOf("@");
    var res = email.substring(index_of_at, -index_of_at);
    if ((pos >= 'a' && pos <= 'z') || (pos >= 'A' && pos <= 'Z')) {

        if (count > 1) {
            document.getElementById("emailError").innerHTML = "Max 1 @ be used  be used";
            document.getElementById("email").value = res;
        }

        /* if (count_space > 1) {
             document.getElementById("emailError").innerHTML = "no space allowed";
         }*/

    } else {
        document.getElementById("emailError").innerHTML = "email should start with a valid character";
        document.getElementById("email").value = ""

    }
}
/////////////////////////////Client Side Validation for Email Feild ends here



/////////////////////////////Client Side Validation for Password Feild starts here
function passwordValid() {
    var password = document.getElementById("password").value;
    var pos = password.charAt(0);
    var str = password.substring(1, 5);

    // var match = str.match(/[a - z]/g);


    if (!(pos >= 'A' && pos <= 'Z')) {
        document.getElementById("passwordError").innerHTML = "First Letter be Capitalized.";
        document.getElementById("password").value = "";
    }

}
/////////////////////////////Client Side Validation for Password Feild ends here\



/////////////////////////////Client Side Validation for Image Feild starts
function show(input) {
    debugger;
    var validExtensions = ['jpg', 'png', 'jpeg']; //array of valid extensions
    var fileName = input.files[0].name;
    var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
    if ($.inArray(fileNameExt, validExtensions) == -1) {
        input.type = ''
        input.type = 'file'
        $('#user_img').attr('src', "");
        alert("Accepted file formats : " + validExtensions.join(', '));
    } else {
        if (input.files && input.files[0]) {
            var filerdr = new FileReader();
            filerdr.onload = function(e) {
                $('#user_img').attr('src', e.target.result);
            }
            filerdr.readAsDataURL(input.files[0]);
        }
    }
}
/////////////////////////////Client Side Validation for image Feild end here

function funOnClick() {
    var name = document.getElementById("name").value;
    var email = document.getElementById("email").value;
    var password = document.getElementById("password").value;
    var address = document.getElementById("address").value;
    var designation = document.getElementById('designation').selectedIndex;
    var image = document.getElementById("file").value;
    if (name == "" || email == "" || password == "" || address == "" || designation == 0 || image == "") {
        if (name == "") {
            document.getElementById("nameError").innerHTML = "Name Feild must not be left empty";


        }
        if (email == "") {
            document.getElementById("emailError").innerHTML = "Email Feild must not be left empty";


        }
        if (password == "") {
            document.getElementById("passwordError").innerHTML = "Password Feild must not be left empty";


        }
        if (address == "") {
            document.getElementById("addressError").innerHTML = "Address Feild must not be left empty";


        }
        if (designation == 0) {
            document.getElementById("designationError").innerHTML = "Designation to be selected";
        }
        if (image == "") {
            document.getElementById("imageError").innerHTML = "Profile picture to be selected";
        }
        return false
    } else {
        return true;
    }

}