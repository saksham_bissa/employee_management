<?php
include 'config.php';
?>
<?php
//Delete queries block starts
@$message=$_GET['message'];
@$action=$_GET['action'];
if($action =='delete')
{
$did=$_GET['did'];
$q="DELETE FROM `registration` WHERE `id`='".$did."' ";
mysqli_query($conn,$q);
}
//delete query block starts.
?>
<?php //Code block for pagination
$query1=mysqli_query($conn,"SELECT * FROM `registration` ");
$row=mysqli_num_rows($query1);
$num_rows=$row;
$row=$row/8;
$row=ceil($row);
@$init_count=1;
if(isset($_GET['page']))
{
$page=$_GET['page'];
if($page==""||$page==1)
{
$p=0;
@$init_count=1;
}
else
{

$p=$page;
$p=($p-1)*8;
$init_count=$p+1;
}
$q="SELECT * FROM `registration` LIMIT  $p,8";  
}
else
{
$q="SELECT * FROM `registration` LIMIT  0,8";
}
?>
<!DOCTYPE html>
<html>
<head>
<title>Employee Listing </title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<style>
.form {
float:center;
}
.error
{
color:red;
}
</style>
</head>
<body>
<?php include 'header2.php'?>
<header>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-sm-12">

            
               <center><h3><u>List of Registered Employees</u></h3></center>
               <center style="background-color:green;color:whitesmoke;padding:.1px"><h3 class=""><b><?php echo $message;  ?></b></h3></center>
            </div>
        </div>
    </div> 
</header>
<section class="content_wrapper">
<div class="container-fluid">
      <div class="row">
          <div class="col-md-12 colo-sm-12" style="font-size:30px;text-decoration:underline;">
	           	<?php
		           for($i=1;$i<=$row;$i++)
                {
	              ?>
                 <a   href="employee_listing.php?page=<?php echo $i;?>" > <?php echo $i ; ?> </a> 
                <?php
                }
               ?>
          </div>
      </div>    
  </div>
    <div class="container-fluid">
       <div class="row">
           <div class="col-md-12 col-sm-12">
    <table class="table table-striped table-hover ">
    <thead class="bg-success">
      <tr>
        <th>Sno.</th>
      <th>Employee Id</th>
        <th>Image</th>
        <th>Name</th>
        <th>Email</th>
        <th>Gender</th>
        <th>Designation</th>
        <!--<button class="btn btn-success" >View</button>  -->
        <th>View</th>
      <!--  <th>Delete</th>-->
      </tr>
    </thead>
    <tbody>
 <?php
 $d1=mysqli_query($conn,$q);
 $counter=0;
 while ($res3=mysqli_fetch_array($d1))
 {
 ?>
      <tr>
        <td><?php echo @$init_count+$counter++; ?></td>
        <td><?php echo $res3['id']?></td>
        <td><img src="<?php echo $res3['image']?>" style="height:100px;width:100px"/></td>
        <td><?php echo $res3['name']?></td>
        <td><?php echo $res3['email']?></td>
        <td><?php echo $res3['gender']?></td>
        <td><?php echo $res3['designation']?></td>
        <td><a class="btn btn-success" href="view-employee-single.php?did=<?php echo $res3['id'];?>&action=view&page=registration"><i class="fa fa-edit"></i>View Profile</a></td>
 
      <!--  <td class=" ">
<a onclick="return confirm('Are you sure?')" class="btn btn-round btn-danger"  style="color:#fff;" href="?did=<?php echo $res3['id'];?>&action=delete">Delete</td>-->
      </tr>
<?php
}
$len=$counter-1;
?>
    </tbody>
  </table>  
           </div>
           <div class="col-md-12 col-sm-12">
          <p style="font-size:1.5em;color:blue">Showing <b><?php echo @$init_count;?>-<?php echo $init_count+$len;?></b> of <b> <?php echo $num_rows; ?> </b> results. </p>
           </div>
       </div>
    <div>
<section>
<?php include 'footer2.php'?>
</body>
</html>
