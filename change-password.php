<?php //Session Block Starts here
session_start();
if($_SESSION["email"]=='')
{
    header("location:login.php");
}

//Session Block Starts here

?>
<?php
$password=$renew_password=$renew_password="";  //initialization of php variables
$passwordErr=$renew_passwordErr=$new_passwordErr="";   //Initialization of error variables;
if ($_SERVER["REQUEST_METHOD"] == "POST") {
 

    if (empty($_POST["password"])) {              //validation of password feild
    $passwordErr = "Password is required";
  } else {
        $pswd_len=strlen($_POST["password"]);     //checking the length of password
         if($pswd_len < 6 || $pswd_len >10) {
        $passwordErr = "Password be alpha numeric of Length between(6-10)";
   
}
         else {
         $temp_pass=$_POST['password'];
         $index=substr("$temp_pass",0);       //fetching first character of the pasword
         $cap=ucwords($index);
         if($index >='A' && $index<='Z')        //checking first character of password is a Upper case alphabet.
         $password= test_input($_POST["password"]);
         else
         $passwordErr = "Password should be in alpha numeric style with Capital letter at the begining";
          
}      
  }
  


  if (empty($_POST["new_password"])) {              //validation of password feild
    $new_passwordErr = "Password is required";
  } else {
        $pswd_len=strlen($_POST["new_password"]);     //checking the length of password
         if($pswd_len < 6 || $pswd_len >10) {
        $new_passwordErr = "Password be alpha numeric of Length between(6-10)";
   
}
         else {
         $temp_pass=$_POST['new_password'];
         $index=substr("$temp_pass",0);       //fetching first character of the pasword
         $cap=ucwords($index);
         if($index >='A' && $index<='Z')        //checking first character of password is a Upper case alphabet.
         $new_password= test_input($_POST["new_password"]);
         else
         $new_passwordErr = "Password should be in alpha numeric style with Capital letter at the begining";
          
}      
  }

  if (empty($_POST["renew_password"])) {              //validation of password feild
    $renew_passwordErr = "Password is required";
  } else {
        $pswd_len=strlen($_POST["renew_password"]);     //checking the length of password
         if($pswd_len < 6 || $pswd_len >10) {
        $renew_passwordErr = "Password be alpha numeric of Length between(6-10)";
   
}
         else {
         $temp_pass=$_POST['renew_password'];
         $index=substr("$temp_pass",0);       //fetching first character of the pasword
         $cap=ucwords($index);
         if($index >='A' && $index<='Z')        //checking first character of password is a Upper case alphabet.
         $password= test_input($_POST["renew_password"]);
         else
         $renew_passwordErr = "Password should be in alpha numeric style with Capital letter at the begining";
          
}      
  }


}
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}//server validation ends
?> 
<?php //php code block for changing the password starts here
include_once 'config.php';
extract($_POST);
@$did=$_GET["id"];
$sql_query="SELECT `password` FROM `registration` WHERE `id` = '".$did."' ";
$d1=mysqli_query($conn,$sql_query);
$res=mysqli_fetch_array($d1);
$dbpass=$res['password'];
if(isset($_POST['change_pass_submit']) && (!empty($_POST['password'])) && (!empty($_POST['new_password'])) && (!empty($_POST['renew_password'])))
{
$password=md5($_POST['password']);
$new_password=md5($_POST['new_password']);
$renew_password=md5($_POST['renew_password']);
     if($dbpass==$password) {
         if($new_password==$renew_password)
         {
             if($new_password==$password)
             {
              $new_passwordErr="Existing Password Cannot be set as new Password";
             }
             else {
                $change_pass_query="UPDATE `registration` SET `password` = '".$new_password."' WHERE `id` = '".$did."' ";
                $d2=mysqli_query($conn,$change_pass_query);
                echo '<script>alert("Password Changed Successfully..Please Login with new password..");
                 window.location.href="logout.php";
                </script>
                '; 
             }
         }
         else
         {
             $renew_passwordErr="Re-entered password does not matched with the New Password";
         }

}
else {
 $passwordErr="Incorrect Password entered.Please enter the correct password";
}
}
//php code for changing the password ends here
?>
<!DOCTYPE html>
<html>
<head>
<title>Change Password</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
.form {
float:center;
}
.error
{
color:red;
}
</style>
</head>
<body>
    <?php include 'header.php'; ?>

<section class="content_wrapper">
      <center><b>Change password</b></center>
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-6" style="margin-left:270px;padding-top:50px">
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>?id=<?php echo $did ?>" method="post">

                    <div class="form-group">
                        <label>
                        Enter Current Password <span class="error">* </span>
                        </label>
                        <input type="password" class="form-control" placeholder="Your Current password" name="password"/>
                        <span class="error"><?php echo $passwordErr;?></span>
                    </div>
                    <div class="form-group">
                        <label>
                        Enter New Password <span class="error">* </span>
                        </label>
                        <input type="password" class="form-control" placeholder="Your New password" name="new_password"/>
                        <span class="error"><?php echo $new_passwordErr;?></span>
                    </div>
                    <div class="form-group">
                        <label>
                       Re-Enter new Password <span class="error">* </span>
                        </label>
                        <input type="password" class="form-control" placeholder="Re-enter new password" name="renew_password"/>
                        <span class="error"><?php echo $renew_passwordErr;?></span>
                    </div>
                    <div class="form-group">

                        <input type="submit" class="form-control btn btn-success" value="Submit"  name="change_pass_submit"/>
                    </div>
                </form>

            </div>
 
        </div>

    </div> 


</section>
<?php include 'footer.php'?>
</body>
</html>