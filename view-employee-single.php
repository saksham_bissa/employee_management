 
<?php
include 'config.php';
@$action=$_GET["action"];
@$did=$_GET["did"];
@$page=$_GET["page"];

if($action=="view")
{
$sel_query="SELECT * FROM `$page` WHERE `id` = '".$did."' ";
$d1=mysqli_query($conn,$sel_query);
$data=mysqli_fetch_array($d1);
}
?>

<!DOCTYPE html>
<html>
<head>
<title>Employee Details Single </title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<style>
.form {
float:center;
}
.error
{
color:red;
}

</style>
</head>
<body>
<?php include 'header2.php'?>
<header>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
               <center><h3>Single detailed view</h3></center>
            
            </div>
        </div>
    </div>
</header>
<section class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <strong><h2><?php echo $data['name'];  ?></h2></strong>
                <br>
                <strong><h3><?php echo $data['designation'];  ?></h3></strong>
            </div>
            <div class="col-md-3 ">
            <img src="<?php echo $data['image']?>" alt="preview not available" title="<?php echo $data['name'];  ?>" class="img-responsive" style="height:230px;width:300px;" />           
         
            </div>

        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                 <table class="table table-responsive table-hover ">
                    <thead>

                    </thead>
                    <tbody>
                        <tr>
                            <td>Employee Id:-</td>
                            <td><?php echo $data['id'];  ?></td>
                        <tr>
                        <tr>
                            <td>Email:- </td>
                            <td><?php echo $data['email'];  ?></td>
                        <tr>
                     <!--   <tr>
                            <td>Password:- </td>
                            <td><?php echo $data['password'];  ?></td>
                        <tr>-->
                        <tr>
                            <td>Gender:- </td>
                            <td><?php echo $data['gender'];  ?></td>
                        <tr>
                        <tr>
                            <td>Hobbies:- </td>
                            <td><?php echo $data['hobbies'];  ?></td>
                        <tr>
                        <tr>
                            <td>Address:- </td>
                            <td><?php echo $data['address'];  ?></td>
                        <tr>
                    <tbody>
                </table>
              <center>  
             <?php
             $prev_id=$data['id']-1;
             $next_id=$data['id']+1;
            ?>
            
            
             <hr>
             <a class="btn btn-success" href="?did=<?php echo $prev_id;?>&action=view&page=registration" style="float:left"><i class="fa fa-edit"></i>Previous</a>
           <!--   <a class="btn btn-primary" name="update" href="edit-employee-details.php?did= <?php echo $data['id'];?>&action=edit&page=registration"><i class="fa fa-edit"></i>Update</a>-->
             
             <a class="btn btn-success" href="?did=<?php echo $next_id;?>&action=view&page=registration" style="float:right;box-radius:none;"><i class="fa fa-edit"></i>Next</a>

             </center>
            </div>
        </div>
    </div>
</section>
<?php include 'footer2.php'?>