<?php //validation block starts  
$name=$email=$gender=$password=$designation=$hobbies=$address=$image="";  //initialization of php variables
$nameErr=$emailErr=$genderErr=$passwordErr=$designationErr=$hobbiesErr=$addressErr=$imageErr="";   //Initialization of error variables;
include 'config.php';
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["name"])) {       //condition if the name feild is empty
    $nameErr = "Name is required";
  } else {
          $name=$_POST["name"];
      if(gettype($name)!=="string") {    //if name feild is not of string type
          $nameErr = "Only Characters allowed.";
  }
          else {
          $name = test_input($_POST["name"]);
  }
   
  }
    if (empty($_POST["email"])) {           //check if the email feild is empty
    $emailErr = "Email is required";
  } 
       else {
         $email=$_POST["email"];
         $count1=substr_count("$email","@");  //count no. of occurance of @
         $count2=substr_count("$email",".");  //count no. of occurance of .
         if($count2 >=3) {
         $emailErr = "Invalid Email Format..";
  }
             else {
           $email=test_input($_POST["email"]);
  }
  }

 /*
 if (empty($_POST["gender"])) {                //validation of gender feild
    $genderErr = "Gender is required";
    
  } else {
    $gender= test_input($_POST["gender"]);
  }
*/

    if (empty($_POST["password"])) {              //validation of password feild
    $passwordErr = "Password is required";
  } else {
        $pswd_len=strlen($_POST["password"]);     //checking the length of password
         if($pswd_len < 6 || $pswd_len >10) {
        $passwordErr = "Password be alpha numeric of Length between(6-10)";
   
}
         else {
         $temp_pass=$_POST['password'];
         $index=substr("$temp_pass",0);       //fetching first character of the pasword
         $cap=ucwords($index);
         if($index >='A' && $index<='Z')        //checking first character of password is a Upper case alphabet.
         $password= test_input($_POST["password"]);
         else
         $passwordErr = "Password should be in alpha numeric style with Capital letter at the begining";
          
}      
  }

     if ($_POST["designation"]=="NA") {          //designation feild validation
    $designationErr = "Designation is required";
  } else {
    $designation= test_input($_POST["designation"]);
  }

    if (empty($_POST["address"])) {                 //address feild validation
    $addressErr = "Address is required";
  } else {
    $address= test_input($_POST["address"]);
  }
   if (empty($_FILES["image"]["name"])) {                  //image feild validation
    $imageErr = "Profile pic required ";
  } else {
    
    $path = $_FILES['image']['name'];
    $ext = pathinfo($path, PATHINFO_EXTENSION);
    if($ext=="jpg" || $ext=="png" ||$ext=="jpeg")       //image extention validation
    $image= test_input($_FILES["image"]["name"]);
    else
    $imageErr = "Please Upload jpg/png format image only ";
  }

if (empty($_POST["hobbies"])) {                        //hobbies feild validation
    $hobbiesErr = "Hobbies are required ";
  } 
//$image=test_input($_POST["image"]);
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
} //validation block ends
?>

<?php //Update Query Block Starts
$did=$_GET['did'];
extract($_POST);
if(isset($_POST["update"]) && (!empty($_POST["name"]))&& (!empty($_POST["email"])) && (!empty($_POST["password"])) && (!empty($_POST["hobbies"]))  && (!empty($_POST["address"])) )
{
       $hobbies=implode(',', $_POST['hobbies']);
	$path = "images/".$_FILES['image']['name'];
    	move_uploaded_file($_FILES['image']['tmp_name'], $path);
	if ($_FILES['image']['tmp_name'] != '' ) {
       	$query="UPDATE `registration` SET `id`='".$did."', `image`='".$path."',`name`='".$name."',`email`='".$email."',`gender`='".$gender."' ,`password`='".$password."' ,`designation`='".$designation."',`hobbies`='".$hobbies."' ,`address`='".$address."' WHERE `id`='".$did."'";
	mysqli_query($conn,$query) or die(mysqli_error());
	echo'<script>
        alert("Details Updated Successfully");
        window.location.href="manage-employee.php";
        </script>';
	
    }	
	if ($_FILES['image']['tmp_name'] == '' ) {
       $query="UPDATE `registration` SET `id`='".$did."',`name`='".$name."',`email`='".$email."',`gender`='".$gender."' ,`password`='".$password."',`designation`='".$designation."',`hobbies`='".$hobbies."' ,`address`='".$address."'  WHERE `id`='".$did."' ";
	mysqli_query($conn,$query) or die(mysqli_error());
	echo'<script>
        alert("Details Updated Successfully");
        window.location.href="manage-employee.php";
       </script>';
    }		
}
//update Query Block Ends
?>
<?php //value fetching block starts 

$sel_query="SELECT * FROM `registration` WHERE `id` = '".$did."' ";
$d1=mysqli_query($conn,$sel_query);
$res=mysqli_fetch_array($d1);
 //value fetching block ends
?>
<?php 

include 'header.php';
?>
    <header>
        <div class="container">
             <div class="row">
                <div class="col-md-12 col-sm-12">
                  <center><h3>Update Employee Details</h3></center>
                
          
                 </div> 
                  <div class="col-md-3 col-sm-12"></div>
                  <div class="col-md-6 col-sm-6 form">
                  
                     <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>?did=<?php echo $res['id'];?>" method="post" enctype="multipart/form-data">
                         <div class="form-group">
                             <label for="id">
                              Employee Id
                             </label>
                             <input type="number" class="form-control" value="<?php echo $res['id'];?>" readonly="readonly"/>
                             <span class="error"> <?php echo $nameErr;?></span>
                         </div>
                         <div class="form-group">
                             <label for="name">
                              Edit Name <span class="error">* </span>
                             </label>
                             <input type="name" id="name" name="name" placeholder="Your Name" class="form-control" value="<?php echo $res['name'];?>"/>
                             <span class="error"> <?php echo $nameErr;?></span>
                         </div>
                         <div class="form-group">
                             <label for="Email">
                              Edit Email Id <span class="error">* </span>
                             </label>
                             <input type="email" id="email" name="email" placeholder="Your Email" class="form-control" value="<?php echo $res['email'];?>"/>
                             <span class="error"><?php echo $emailErr;?></span>
                         </div>
                         <div class="form-group">
                             <label for="gender">
                              Select Gender <span class="error"> </span>
                             </label>
                              <br>
                             <input type="radio" id="gender"  name="gender" value="Male" <?php if($res['gender']=="Male") { ?>checked="checked"<?php }   ?>/>Male
                             <input type="radio" id="gender" name="gender" value="Female" <?php if($res['gender']=="Female") { ?>checked="checked"<?php }   ?>/>Female
                         </div>
                         <div class="form-group">
                             <label for="password">
                              Edit Password<span class="error">* </span>
                             </label>
                             <input type="text" id="password" name="password" placeholder="Your Password" class="form-control" value="<?php echo $res['password'];?>"/>
                             <span class="error"><?php echo $passwordErr;?>
                         </div>
                         <div class="form-group">
                             <label for="designation">
                              Edit Designation<span class="error">* </span>
                             </label>
                             <select class="form-control" name="designation">
                                 <option selected="false" value="<?php echo $res['designation'] ?>" ><?php echo $res['designation'] ?></option>
                                 <option value="Software Engineer">Software Engineer</option>
                                 <option value="Sr. Software Engineer">Sr. Software Engineer</option>
                                 <option value="Project Lead">Project Lead</option>
                                 <option value="Tech. Lead">Tech. Lead</option>
                                 <option value="System Analyst">System Analyst</option>
                             </select>
                             <span class="error"><?php echo $designationErr;?></span>
                         </div>
                         <div class="form-group">
                             <label for="hobbies">
                              Choose Your Hobbies <span class="error">*</span>
                             </label>
                             <br>
                            <?php
                                 $query="SELECT * FROM `registration` WHERE `id` = '".$did."' ";
                                 $d2=mysqli_query($conn,$sel_query);
                                 while($row = mysqli_fetch_array($d2))
                                  {
                                 $focus = explode(",", $row['hobbies']);
                                 
                              ?>   
                         
                             
                   <input type="checkbox" id="hobbies" name="hobbies[]" value="Playing" <?php if(in_array("Playing",$focus)) { ?> checked="checked" <?php } ?>/>Playing
                   <input type="checkbox" id="hobbies" name="hobbies[]" value="singing" <?php if(in_array("singing",$focus)) { ?> checked="checked" <?php } ?>/>Singing
                   <input type="checkbox" id="hobbies" name="hobbies[]" value="Dancing"<?php if(in_array("Dancing",$focus)) { ?> checked="checked" <?php } ?>/>Dancing
                   <input type="checkbox" id="hobbies" name="hobbies[]" value="Watching Movies" <?php if(in_array("Watching Movies",$focus)) { ?> checked="checked" <?php } ?>/>Watching Movies
                   <input type="checkbox" id="hobbies" name="hobbies[]" value="Reading Novels" <?php if(in_array("Reading Novels",$focus)) { ?> checked="checked" <?php } ?>/>Novels
                   <input type="checkbox" id="hobbies" name="hobbies[]" value="cooking" <?php if(in_array("cooking",$focus)) { ?> checked="checked" <?php } ?>/>Cooking
                            <?php 
                             }
                            ?>
                             <span class="error"><?php echo $hobbiesErr;?></span>
                         </div>
                         <div class="form-group">
                             <label for="address">
                              Edit Communication Address<span class="error">* </span>
                             </label>
                             <textarea name="address" id="address" class="form-control" ><?php echo $res['address'];?></textarea>
                            
                             <span class="error"><?php echo $addressErr;?></span>
                         </div>
                         <div class="form-group">
                             <label for="image">
                              Edit Profile Picture <span class="error">* </span>
                             </label>
                             <input type="file" name="image" id="image" class="form-control" value="<?php echo $_FILES["image"]["name"];  ?>"/>
                            <span class="error"> <?php echo $imageErr;?></span>
                         </div>
                         <div class="form-group">

                         <button type="submit" class="btn btn-success" name="update" >Update Details</button>
                         </div>
                     </form>
                 
                 </div>
             </div>
        </div>
      
    </header>

<body>


</html>
