<?php session_start(); ?>
<?php
$name=$email=$gender=$password=$designation=$hobbies=$address=$image="";  //initialization of php variables
$nameErr=$emailErr=$genderErr=$passwordErr=$designationErr=$hobbiesErr=$addressErr=$imageErr="";   //Initialization of error variables;
if ($_SERVER["REQUEST_METHOD"] == "POST") {
 
    if (empty($_POST["email"])) {           //check if the email feild is empty
    $emailErr = "Email is required";
  } 
       else {
         $email=$_POST["email"];
         $count1=substr_count("$email","@");  //count no. of occurance of @
         $count2=substr_count("$email",".");  //count no. of occurance of .
         if($count2 >=3) {
         $emailErr = "Invalid Email Format..";
  }
             else {
           $email=test_input($_POST["email"]);
  }
  }

    if (empty($_POST["password"])) {              //validation of password feild
    $passwordErr = "Password is required";
  } else {
        $pswd_len=strlen($_POST["password"]);     //checking the length of password
         if($pswd_len < 6 || $pswd_len >10) {
        $passwordErr = "Password be alpha numeric of Length between(6-10)";
   
}
         else {
         $temp_pass=$_POST['password'];
         $index=substr("$temp_pass",0);       //fetching first character of the pasword
         $cap=ucwords($index);
         if($index >='A' && $index<='Z')        //checking first character of password is a Upper case alphabet.
         $password= test_input($_POST["password"]);
         else
         $passwordErr = "Password should be in alpha numeric style with Capital letter at the begining";
          
}      
  }

}
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}//feild validation ends
?>
<?php //Login Query Block starts
include 'config.php';
extract($_POST);
if(isset($_POST['login_submit']) && (!empty($_POST['email'])) && (!empty($_POST['password'])) )
{
    $password=md5($password);
    $log_query="SELECT `email`,`password` FROM `registration` WHERE `email` = '".$email."' and `password`= '".$password."'  ";
    $data=mysqli_query($conn,$log_query);
    $result=mysqli_fetch_array($data);
    if($email== $result["email"] and $password ==$result["password"] )
    {
        $_SESSION["email"]=$email;
        $_SESSION["password"]=$password;
        header("location:dashboard.php");
    }
    else
    {
    
        echo'<script>alert("login failed ");</script>';
    }   
}
//login Query Block ends here.
?> 
<!DOCTYPE html>
<html>
<head>
<title>Home</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<style>
.form {
float:center;
}
.error
{
color:red;
}
</style>
</head>
<body>
<header>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
               <center><h1>Employee Login Panel</h1></center>
               <br>
              <b> <hr></b>

               <b> <hr></b>
               
            </div>
        </div>
    </div>
</header>
<section class="content_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-6" style="margin-left:270px;">
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
                    <div class="form-group">
                        <label>
                         Enter Email Id <span class="error">* </span>
                        </label>
                        <input type="email" class="form-control" placeholder="Your Email id" name="email"/>
                        <span class="error"><?php echo $emailErr;?></span>
                    </div>
                    <div class="form-group">
                        <label>
                        Enter Password <span class="error">* </span>
                        </label>
                        <input type="password" class="form-control" placeholder="Your password" name="password"/>
                        <span class="error"><?php echo $passwordErr;?></span>
                    </div>
                    <div class="form-group">

                        <input type="submit" class="form-control btn btn-success" value="Secure Login !"  name="login_submit"/>
                    </div>
                </form>

            </div>
 
        </div>
        <hr>
                   <center><h3><strong ><a href="form.php"  >Click Here for New Registration ! </a></strong></h3></center>
        <hr>
    </div> 


</section>
</body>
</html>