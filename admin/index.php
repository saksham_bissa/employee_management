<?php session_start(); ?>
<?php  //code for storing the cookies for admin starts here
function set_Cookies($a)
{
    $cookie_name="admin_cookie";
    $cookie_value=$a;
    setcookie($cookie_name,$cookie_value,time()+86400,"/");
   
if(isset($_COOKIE[@$cookie_name]))
{
  // echo $_COOKIE[$cookie_name]." is set";
   header("location:admin.php");
}
else {
    
  //  echo "cookie not set";
}
}
set_Cookies(@$_SESSION["username"]);
 //code for storing the cookies for admin starts here
?>
<?php
$username=$password="";//initialization of php variables
$usernameErr=$passwordErr="";//Initialization of error variables;
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["username"])) {           //check if the username is empty
    $usernameErr = "Username Required";
  } 
       else {
      
           $username=test_input($_POST["username"]);
  }
  

    if (empty($_POST["password"])) {              //validation of password feild
    $passwordErr = "Password is required";
  } else {
        $pswd_len=strlen($_POST["password"]);     //checking the length of password
         if($pswd_len < 6 || $pswd_len >10) {
        $passwordErr = "Password be alpha numeric of Length between(6-10)";
   
}
         else {
         $temp_pass=$_POST['password'];
         $index=substr("$temp_pass",0);       //fetching first character of the pasword
         $cap=ucwords($index);
         if($index >='A' && $index<='Z')        //checking first character of password is a Upper case alphabet.
         $password= test_input($_POST["password"]);
         else
         $passwordErr = "Password should be in alpha numeric style with Capital letter at the begining";
          
}      
  }

}
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}//feild validation ends
?>
<?php //Login Query Block starts
include 'config.php';
extract($_POST);
if(isset($_POST['admin_submit']) && (!empty($_POST['username'])) && (!empty($_POST['password'])) )
{
    
    $log_query="SELECT `username`,`password` FROM `admin` WHERE `id` =1";
    $data=mysqli_query($conn,$log_query);
    $result=mysqli_fetch_array($data);
   // echo $result['username'].$result['password'];
    if($username==$result["username"] and $password ==$result["password"])
    {
        $_SESSION["username"]=$username;
        $_SESSION["password"]=$password;
        header("location:admin.php");
        
    }
    else
    {
    
        echo'<script>alert("Unauthorised Access..");</script>';
    }   
}
//login Query Block ends here.
?> 
<!DOCTYPE html>
<html>
<head>
<title>Admin Panel </title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<style>
.form {
float:center;
}
.error
{
color:red;
}
</style>
</head>
<body>
    <?php include '../header2.php'?>
<header>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
               <center><h3>Admin Login Panel</h3></center>
             
               
            </div>
        </div>
    </div>
</header>
<section class="content_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-6" style="margin-left:270px;">
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
                    <div class="form-group">
                        <label>
                        Username <span class="error">* </span>
                        </label>
                        <input type="text" class="form-control" placeholder="Your Username" name="username"/>
                        <span class="error"><?php echo $usernameErr;?></span>
                    </div>
                    <div class="form-group">
                        <label>
                        Password <span class="error">* </span>
                        </label>
                        <input type="password" class="form-control" placeholder="Your password" name="password"/>
                        <span class="error"><?php echo $passwordErr;?></span>
                    </div>
                    <div class="form-group">

                        <input type="submit" class="form-control btn btn-success" value="Secure Login !"  name="admin_submit"/>
                    </div>
                </form>

            </div>
 
        </div>
       
    </div> 


</section>
<?php include '../footer2.php'?>
</body>
</html>